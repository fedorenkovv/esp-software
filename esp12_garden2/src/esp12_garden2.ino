#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <JC_Button.h>
#include <Shifty.h>
#include <DHT.h>
#include <FS.h>

#include <VVF.h>
#include "wifi_auth.h"

#ifdef ENABLE_OTA
#include <ESP8266HTTPUpdateServer.h>
#endif

const char* ssid = WIFI_SSID;
const char* password = WIFI_PASSWORD;

ESP8266WebServer server(80);
#ifdef ENABLE_OTA
ESP8266HTTPUpdateServer httpUpdater;
#endif

#define VALVE_COUNT 4
#define STATUS_OFF 0
#define STATUS_ON 1
#define STATUS_GETTING_OFF 2
#define STATUS_GETTING_ON 3

bool turning = false;
uint8_t statuses[] = {0,0,0,0};
uint32_t wait_loops[] = {0,0,0,0};
uint32_t _ms = millis();
struct Config {
  uint16_t valveDelay;
  uint16_t analogThresholdUp;
  uint16_t analogThresholdDown;
  uint16_t analogDelay;
  uint16_t tempDelay;
} config = {300, 1000, 500, 10, 60};

#define DEBOUNCE_MS 35
#define INIT_BUTTON(pin) Button(pin, true, false, DEBOUNCE_MS)
Button buttons[]={
  INIT_BUTTON(5),
  INIT_BUTTON(4),
  INIT_BUTTON(0),
  INIT_BUTTON(10)
};

Shifty shift;

#define DHT22_PIN 16
// Setup a DHT22 instance
DHT myDHT22;


VVFsWebAPI webapiServer(ssid, password);

void setStatus(uint8_t devNo, int value){
  DEBUGLOG(String("setStatus ") + devNo+" "+ value);
  shift.batchWriteBegin();
  if(value){
    statuses[devNo] = STATUS_GETTING_ON;
    // turn power on +-
    shift.writeBit(devNo*2,   false);
    shift.writeBit(devNo*2+1, true);
  }else{
    statuses[devNo] = STATUS_GETTING_OFF;
    // turn power on -+
    shift.writeBit(devNo*2,   true);
    shift.writeBit(devNo*2+1, false);
  }
  shift.batchWriteEnd();

  wait_loops[devNo] = millis()+config.valveDelay;
};

int getStatus(uint8_t devNo){
  DEBUGLOG("get status");
  DEBUGLOG(statuses[devNo]);
  return (int)statuses[devNo];
}

const char * configFilePath="/config.bin";

void loadConfig(){
  if(!SPIFFS.exists(configFilePath)){
    return;
  }

  File f = SPIFFS.open(configFilePath, "r");

  f.read((uint8_t *)&config, sizeof(config));

  f.close();
}

void saveConfig(){
  File f = SPIFFS.open(configFilePath, "w");
  f.write((uint8_t *)&config, sizeof(config));
  f.close();
}
bool parseOneCfgParam(String name, uint16_t &pparam, String& resp){
  String val = server.arg(name);
  uint32_t oldValue = pparam;
  if(val.length()){
    pparam = val.toInt();
  }
  resp += '"';
  resp += name;
  resp += "\":";
  resp += pparam;
  return oldValue != pparam;
}

void handleConfig() {

  String resp = "";
  bool hasChanges;
  String val;
  hasChanges = parseOneCfgParam("ato", config.analogDelay, resp);
  resp += ',';
  if(parseOneCfgParam("aup", config.analogThresholdUp, resp)){ hasChanges = true;  }
  resp += ',';
  if(parseOneCfgParam("adn", config.analogThresholdDown, resp)){ hasChanges = true;  }
  resp += ',';
  if(parseOneCfgParam("tto", config.tempDelay, resp)){ hasChanges = true;  }
  resp += ',';
  if(parseOneCfgParam("vto", config.valveDelay, resp)){ hasChanges = true;  }

  if(hasChanges){
    saveConfig();
    resp += ",\"saved\":true";
  }
  server.send(200, "text/json", "{ " + resp + ",\"success\":true }");
  yield();
  webapiServer.broadcastOff();
}

void setup(){

  Serial.begin(115200);

  #ifdef DEBUG
  delay(5000);
  Serial.println("Start");
  #endif
  pinMode(5, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(0, INPUT_PULLUP);
  pinMode(10, INPUT_PULLUP);
  digitalWrite(5, true);
  digitalWrite(4, true);
  digitalWrite(0, true);
  digitalWrite(10, true);


  shift.setPins(13, 14, 12);
    //int dataPin, int clockPin, int latchPin [, int readPin]);
  shift.setBitCount(8);

  myDHT22.setup(DHT22_PIN);

  // Serial.println("Effects start effect");
  webapiServer.setup(&server, 4);
  webapiServer.setDeviceFn(setStatus, getStatus);
  webapiServer.setupButtons(4, [&](int8_t btnNo){
    return buttons[btnNo].isReleased() ? 1 : 0;
  });

  #ifdef ENABLE_OTA
  httpUpdater.setup(&server);
  #endif
// /*
  loadConfig();
  #ifdef DEBUG
  Serial.println("init handlers");
  #endif
  server.on("/isGarden1", [&]{
    server.send(200, "text/plain", "No, but...");
    yield();
  });
  server.on("/isGarden2", [&]{
    server.send(200, "text/plain", "Yes");
    yield();
  });
  server.on("/turningMode", [&]{
    if(server.method() == HTTP_POST){
      turning = !turning;
    }
    server.send(200, "text/plain", turning ?  "Yes" : "No");
    yield();
  });
  server.on("/temp", [&]{
    String payload = "{";
    payload += "\"t\":";
    payload += myDHT22.getTemperature();
    payload += ",\"h\":";
    payload += myDHT22.getHumidity();
    payload += "}";
    server.send(200, "text/plain", payload.c_str());
    yield();
  });
  server.on("/restart", [&]{
    if(server.method() == HTTP_POST){
      server.send(200, "text/html", "<h1>Restarting...</h1>");
      yield();
      ESP.restart();
      return;
    }
    server.send(200, "text/html", "<form method='post'><input type='submit' value='restart'></form>");
    yield();
  });
  server.on("/config", handleConfig);

  // */
  yield();
  server.begin();
  for(uint8_t devNo=0; devNo < VALVE_COUNT; devNo++){
    setStatus(devNo, STATUS_OFF);
  }
}

void checkValveOff(){
  uint32_t _ms = millis();
  bool hasChanges = false;
  for(int8_t i =0;i<VALVE_COUNT; i++){
    if(wait_loops[i] && _ms > wait_loops[i]){
      if(!hasChanges){
        shift.batchWriteBegin();
      }
      hasChanges = true;
      shift.writeBit(i*2,   false);
      shift.writeBit(i*2+1, false);
      wait_loops[i] = 0; // disable checking
      statuses[i] &= 1; // remove "-ing" flag
      String event = "valve:done:";
      event += (i+1);
      event += ":";
      event += statuses[i];
      webapiServer.anyNotify(event.c_str());
    }
  }
  if(hasChanges){
    shift.batchWriteEnd();
  }
}

void checkButtons(){
  for(size_t i=0;i<4;i++){
    buttons[i].read();
    bool hasEvent = false;
    String event = "button:";
    if(buttons[i].wasReleased()){
        event += "press:";
        hasEvent = true;
    }else if(buttons[i].wasPressed()){
      event += "release:";
      hasEvent = true;
    }
    if(hasEvent){
      event += (int)(i+1);
      event += ":";
      event += (int)statuses[i];
      webapiServer.anyNotify(event.c_str());
    }
  }
}
#define ANALOG_HIGH config.analogThresholdUp
#define ANALOG_LOW config.analogThresholdDown
uint32_t analogCheckTime = 0;
bool isAnalogHigh = false;
void checkAnalog(){
  uint32_t _ms = millis();
  if(analogCheckTime < _ms){
    int analogValue = webapiServer.getAnalogValue();
    if((isAnalogHigh && analogValue < ANALOG_LOW) || (!isAnalogHigh && analogValue > ANALOG_HIGH)){
      isAnalogHigh = !isAnalogHigh;
      analogCheckTime = _ms + config.analogDelay*1000;
      String payload = "analog:";
      payload += isAnalogHigh ? "up" : "down";
      payload += ":value=";
      payload += analogValue;
      webapiServer.anyNotify(payload.c_str());
    }
  }
}

uint32_t temperatureCheckTime = millis() + 5000;
void checkTemperature(){
  uint32_t _ms = millis();
  if(temperatureCheckTime < _ms){
    myDHT22.getTemperature();
    int errorCode = myDHT22.getStatus();
    temperatureCheckTime = _ms + config.tempDelay*1000;
    if(0 == errorCode){
      String payload = "temp:t=";
      payload += myDHT22.getTemperature();
      payload += ":h=";
      payload += myDHT22.getHumidity();
      webapiServer.anyNotify(payload.c_str());
    }
  }
}

void loop(){
  webapiServer.yield();
  server.handleClient();
  yield();
  checkValveOff();
  checkButtons();
  checkAnalog();
  checkTemperature();
  delay(1);
}
