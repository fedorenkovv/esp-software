#include <Arduino.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <VVF.h>
#include <ESP8266FtpServer.h>
#include <FS.h>
#include <ESP8266HTTPClient.h>

OneWire oneWire(5);
DallasTemperature sensors(&oneWire);
FtpServer ftpSrv;

/*
  Можно вынести в "VVF"
  опрос температуры задается настройкой "период опроса".
  Сначала поставить период опроса = 5 минут.
  посылать событие после считывания температуры на сервер.
*/

/*
  события кнопок, кроме отправки на сервер,
  еще могут быть url на который нужно послать get запрос.
  Это реализовано через SPIFFS: имя файла=<событие><номер_кнопки>.txt
  файлы можно залить по фтп
*/

/*
TODO: опрос аналогового датчика - посылать события
 - при превышении заданного значения М1
 - при снижении ниже заданного значением М2
*/

#ifdef ENABLE_OTA
#include <ESP8266HTTPUpdateServer.h>
#endif

#define EVENT_COUNT 2

#include "wifi_auth.h"
const char* ssid = WIFI_SSID;
const char* password = WIFI_PASSWORD;

uint16_t debounce=200;
enum TButtonEvents {EVENT_UP, EVENT_DOWN} ;
const char* eventNames[]={
  "up", "down"
};

ESP8266WebServer server(80);
#ifdef ENABLE_OTA
ESP8266HTTPUpdateServer httpUpdater;
#endif

VVFsWebAPI webapiServer(ssid, password);
#ifdef DEBUG
unsigned long periodTemperatureAsk = 10000; // 10 sec
#else
unsigned long periodTemperatureAsk = 300000; // 5 min
#endif
unsigned long nextTemeratureAsk=0, whenTemperatureReady=0;
DeviceAddress thermometerAddress;
struct Button{
  bool state;
  const int8_t pin;
  unsigned long reCheckChangesTime;
  char * eventReactions[EVENT_COUNT];

  Button(int8_t p): pin(p), reCheckChangesTime(0){
    for(int i=0;i<EVENT_COUNT; i++){
      eventReactions[i] = NULL;
    }
  }

  void begin(){
    pinMode(pin, pin==16 ? INPUT : INPUT_PULLUP);
    state = digitalRead(pin);
  }

  void setEventReaction(int8_t eventNo, char* reaction){
    eventReactions[eventNo] = reaction;
  }

  void fireChanged(int8_t btnNo){
    int eventNo = state ? EVENT_UP : EVENT_DOWN;
    String reaction(eventReactions[eventNo]);
    String httpSuccess = "";
    if(reaction){
      // do this reaction.
      if(reaction == "disabled"){
        return; // don't sent to server
      }

      if(reaction.length()>4 && reaction.startsWith("URL:")){
        HTTPClient http;
        http.begin(reaction.substring(4));
        int httpCode = http.GET();
        if(httpCode == HTTP_CODE_OK) {
          String payload = http.getString();
          httpSuccess = "http=ok,";
          httpSuccess += payload.length();
          for(int i=0;i < 1e5 && http.connected(); i++){
            delay(10);
            ::yield();
          }
        }else{
          httpSuccess = "http=error,";
          httpSuccess += httpCode;
        }
        http.end();
      }
    }
    ::yield();
    String payload=eventNames[eventNo];
    payload += ":";
    payload += String(1+eventNo, DEC);
    payload += ":";
    payload += btnNo;
    if(httpSuccess){
      payload += ":";
      payload += httpSuccess;
    }
    char buf[payload.length()+1];
    payload.toCharArray(buf, payload.length()+1);
    if(!webapiServer.tcpNotify(buf)){
      webapiServer.udpNotify(buf);
    };
  }

  void read(int8_t btnNo){
    bool newState = digitalRead(pin);
    if(reCheckChangesTime && millis() > reCheckChangesTime){
      reCheckChangesTime = 0;
      if(newState != state){
        // fire event:
        state = newState;
        fireChanged(btnNo);
      }
      return;
    }
    if(!reCheckChangesTime && newState != state){
      reCheckChangesTime = millis() + debounce;
    }
  }
};

#define MAX_CLICKS 16

void react_to_mic();
struct Microphone : public Button{
  /*
  TODO: detect several sequences of "clicks".
  For examle "salsa rythm", zook rythm, kizomba rythm
  */
  unsigned long lastChangeTime;
  unsigned long newChangeTime;
  uint16_t formula[MAX_CLICKS];
  int formulaPosition;
  Microphone(int8_t p): Button(p){
    attachInterrupt(p, react_to_mic, RISING);
    state = false;
    formulaPosition=0;
    lastChangeTime = 0;
    newChangeTime = 0;
  }
  void push(uint16_t newValue) {
    if(formulaPosition>=MAX_CLICKS-1){
      return;
    }
    formula[formulaPosition] = newValue;
    formulaPosition ++;
  }

  void read(int8_t btnNo){
    if(millis() > reCheckChangesTime){
      reCheckChangesTime = millis() + 100;
      if(state){
        fireChanged(btnNo);
      }
      state = false;
    }
    yield();
    if(newChangeTime){
      if(lastChangeTime){
        push(newChangeTime - lastChangeTime);
      }
      lastChangeTime = newChangeTime;
      newChangeTime = 0;
    }
    if(lastChangeTime && millis()-lastChangeTime > 3000){ // ten secons of silence
      if( formulaPosition>1){
        String payload="bits=";
        for(int i=0;i<formulaPosition;i++){
          if(i){
            payload += ",";
          }
          payload += formula[i];
        }
        char buf[payload.length()+1];
        payload.toCharArray(buf, payload.length()+1);
        if(!webapiServer.tcpNotify(buf)){
          webapiServer.udpNotify(buf);
        };
      }
      lastChangeTime = 0;
      formulaPosition = 0;
    }
    return;
  }
};

Microphone mic(4);
void react_to_mic(){
  if(!mic.state){
    mic.newChangeTime = millis();
  }
  mic.state = true;
}


#define BUTTONS_COUNT 4

Button buttons[]{
  Button(12), Button(13), Button(14), //switch - 0, 1, 2
  Button(10) // motion dector (microwave) - 3
   // microphone - 4
};

const char* content_type="text/json";
void setup(){
  Serial.begin(115200);
  pinMode(0, INPUT_PULLUP);
  // This pin is pull down only if chip has been just programmed by serial.
  if(!digitalRead(0)){
    Serial.setDebugOutput(true);
  }

  sensors.begin();
  sensors.setWaitForConversion(false);

  WiFi.begin(ssid, password);
  WiFi.mode(WIFI_STA);

  pinMode(5, INPUT_PULLUP);
  webapiServer.setup(&server, 0);
  webapiServer.setupButtons(BUTTONS_COUNT, [&](int8_t btnNo){
    return buttons[btnNo].state;
  });
  nextTemeratureAsk = millis() + 10000;
  whenTemperatureReady = 0;
  // TODO: setResolution for all sensors


  SPIFFS.begin();
  #ifdef DEBIG
  server.on("/format-fs",[&]{
    if(SPIFFS.exists("/.lock-fmt")){
      server.send(200, content_type, "{ \"success\":false, \"alreadyFormatted\":true }");
      delay(2);
      return;
    }
    if(SPIFFS.format()){
      File f = SPIFFS.open("/.lock-fmt", "w");
      f.write('V');
      f.close();
      server.send(200, content_type, "{ \"success\":true }");
    }else{
      server.send(200, content_type, "{ \"success\":false }");
    }
    yield();
  });
  #endif
/*
  server.on("/temp", [&]{
    int countSensors = sensors.getDeviceCount();
    if(countSensors<=0){
      sensors.begin();
    }
    String resp = "{\"n\":";
    resp += countSensors;
    if(whenTemperatureReady){
      resp += ",\"error\":\"measure\"";
    }else{
      resp += ", [";
      for(int i=0;i<countSensors;i++){
        if(!sensors.getAddress(thermometerAddress, i)){
          continue;
        }
        resp += "{\"address\":\"";
        for(int j=0;j<8;j++){
          resp += String(thermometerAddress[j], HEX);
        }
        resp += "\", \"value\":";
        float temperature = sensors.getTempC(thermometerAddress);
        resp += String(temperature);
        resp += "}";
        if( i ){
          resp += ", ";
        }
      }
      resp += "]";
      nextTemeratureAsk = 1;
    }
    resp += "}";
    server.send(200, content_type, resp);
    yield();
  });
*/
  ftpSrv.begin("vvf", "vvf");
  #ifdef ENABLE_OTA
  httpUpdater.setup(&server);
  #endif
  server.begin();
  // Start buttons and Init event reactions
  for(int8_t i=0; i<BUTTONS_COUNT; i++){
    buttons[i].begin();
    for(int8_t eventNo=0;eventNo < EVENT_COUNT; eventNo++){
      String fileName = "/";
      fileName += eventNames[eventNo];
      fileName += i;
      fileName += ".txt";
      if(SPIFFS.exists(fileName)){
        File f = SPIFFS.open(fileName, "r");
        if(f){
            size_t fileSize = f.size();
            char * reaction = new char[fileSize+1];
            reaction[fileSize] = 0;
            f.readBytes(reaction, fileSize);
            buttons[i].setEventReaction(eventNo, reaction);
            DEBUGLOG("readed reaction:");
            DEBUGLOG(reaction);
            f.close();
        }
      }
    }
  }
}

void handleThermometers(){
  if(whenTemperatureReady && millis() > whenTemperatureReady){
    whenTemperatureReady = 0;
    int countSensors = sensors.getDeviceCount();
    String payload = "temp:n=";
    payload += countSensors;
    for(int i=0;i<countSensors;i++){
      if(!sensors.getAddress(thermometerAddress, i)){
        continue;
      }
      payload += ":t";
      for(int j=0;j<8;j++){
        payload += String(thermometerAddress[j], HEX);
      }
      payload += "=";
      float temperature = sensors.getTempC(thermometerAddress);
      payload += String(temperature);
    }
    if(countSensors>0){
      char *buf = new char[payload.length()+2];
      payload.toCharArray(buf, payload.length()+2);
      if(!webapiServer.tcpNotify(buf)){
        webapiServer.udpNotify(buf);
      };
      delete buf;
    }
    return;
  }
  if( millis() > nextTemeratureAsk){
    nextTemeratureAsk = millis() + periodTemperatureAsk;
    if(!sensors.getDeviceCount()){
      sensors.begin(); // rescan devices
      return;
    }
    DEBUGLOG("start measure temperature");
    sensors.requestTemperatures();
    whenTemperatureReady = millis() + 750;
  }
}

void loop(){
  webapiServer.yield();
  server.handleClient();
  yield();
  ftpSrv.handleFTP();
  yield();
  handleThermometers();
  for(int8_t i=0; i<BUTTONS_COUNT; i++){
    buttons[i].read(i);
    yield();
  }
  mic.read(4);
  yield();
}
/*
void checkIR(){
  if(irrecv.decode(&irResults)){
    String payload = "IR=";
    payload += String(irResults.value, HEX);
    char buf[payload.length()+1];
    payload.toCharArray(buf, payload.length()+1);
    if(!webapiServer.tcpNotify(buf)){
      webapiServer.udpNotify(buf);
    };
    irrecv.resume();
  }
}
*/
