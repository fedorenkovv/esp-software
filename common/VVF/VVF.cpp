#include <VVF.h>
#include <Arduino.h>
//#include <WiFiClient.h>
#include <ESP8266WiFi.h>

#ifdef DEBUG
#warning "debug mode on"
#endif

const char* VVFsWebAPI::contentType = "text/json";
#ifdef DEV_TYPE_PWM
const char* VVFsWebAPI::devTypeStr = "dimmer";
#else
const char* VVFsWebAPI::devTypeStr = "relay";
#endif

WiFiUDP VVFsWebAPI::udp;
WiFiServer VVFsWebAPI::notifySrv(8077);

VVFsWebAPI::VVFsWebAPI(const char *wifiSSID, const char * wifiPassword){
  ssid = wifiSSID;
  password = wifiPassword;
  broadcastNext = 1; // start broadcast
  requestRestartTime=0;
  devCount = 0;
  broadcastDelay = 5000;
  nextSecondAt = 0;
  server = NULL;
  usedButtons = false;
  usedRelays = false;
  nextAnalogMeasureTime = 0;
}

void VVFsWebAPI::broadcastOff(){
  DEBUGLOG("Broadcast off");
  broadcastNext = 0;
}

void VVFsWebAPI::setup(ESP8266WebServer *s, int dc){
  notifySrv.begin();
  server = s;
  devCount = dc;
  if(s != NULL){
    s->on("/reset", [&](){
      handle_reset();
    });
    s->on("/", [&](){
      handle_status();
    });
    s->on("/status", [&](){
      handle_status();
    });
    usedRelays = devCount>0;
    if(usedRelays){
      #ifdef DEV_TYPE_PWM
      s->on("/dimm",
      #else
      s->on("/turn",
      #endif
      [&](){
        handle_turn();
      });
    }
  }
  initWiFi();
}

void VVFsWebAPI::setDeviceFn(TSetDevValueCallback _set, TGetDevValueCallback _get){
  setDevValue = _set;
  getDevValue = _get;
}

void VVFsWebAPI::setupButtons(int8_t bc, TGetDevValueCallback _get){
  buttonsCount = bc;
  getButtonStatus = _get;
  usedButtons = bc>0;
}

void VVFsWebAPI::yield(){
  send_i_am_here();
  checkNotifyConnection();
  if(isWaitingWiFi){
    connectToWiFi();
  }else
  if(WiFi.status() != WL_CONNECTED){
    isWaitingWiFi = true;
  }
  measureAnalog();
}

void VVFsWebAPI::measureAnalog(){
  if(millis()<nextAnalogMeasureTime){
    return;
  }

  nextAnalogMeasureTime = millis() + CHECK_ANALOG_TIMEOUT;
  analogValue = (3*analogValue + analogRead(A0))/4;
}

void VVFsWebAPI::send_i_am_here(){
  udpNotify(NULL);
}

void VVFsWebAPI::udpNotify(char * payload){

  if(!payload &&
    (WiFi.status() != WL_CONNECTED || !broadcastNext || millis() < broadcastNext)
  ){
    return;
  }
  broadcastNext = millis()+broadcastDelay;
  IPAddress addr = WiFi.localIP();
  addr[3] = 255;

  udp.beginPacket(addr, 0xB0BA);
  udp.write("ESP-VVF:");
  char buf[14];
  String(ESP.getChipId(), 16).toCharArray(buf,10);
  udp.write((const char *)buf);
  if(payload){
    udp.write(':');
    udp.write(payload);
  }
  udp.endPacket();
  delay(250);
}

void VVFsWebAPI::handle_reset(){
  uint32_t _ms = millis();
  if(_ms < requestRestartTime){
    server->send(200, contentType, "{\"restarting\":true, \"success\": true}");
    yield();
    delay(750);
    ESP.restart();
    return;
  }
  // confirmation of reset is repeating the request in 2 seconds
  requestRestartTime = _ms+2000;
  server->send(200, contentType, "{\"needConfirm\":true, \"success\": true}");
  yield();
  delay(5);
}

// ---- webserver handlers:
void VVFsWebAPI::handle_status() {
  String resp = "{ ";
  if(usedRelays&&devCount){
    for(short i=0;i<devCount;i++){
      resp += "\"state";
      // resp += devTypeStr;
      resp += i;
      resp += "\": ";
      resp += getDevValue(i);
      resp += ", ";
    }
  }
  resp += "\"type\":\"";
  resp += devTypeStr;
  resp += "\", ";
  if(usedButtons&&buttonsCount){
    for(int8_t i=0;i<buttonsCount;i++){
      resp += "\"button";
      resp += i;
      resp += "\": ";
      resp += getButtonStatus(i);
      resp += ", ";
    }
  }
  resp += "\"analog\":";
  resp += analogValue;
  resp += ", \"flash\":";
  resp += ESP.getFlashChipRealSize();
  resp += ", \"id\":";
  resp += ESP.getChipId();
  resp += ", \"free\":";
  resp += ESP.getFreeHeap();
  resp += ", \"freeSS\":";
  resp += ESP.getFreeSketchSpace();
  resp += ", ";
  resp += "\"success\":true }";
  server->send(200, contentType, resp);

  delay(1);
  ::yield();
  broadcastOff();
}



void VVFsWebAPI::handle_turn() {
  int done = devCount;
  char arg_name_buf[10];
  String resp = "{";
  for(short i=0;i<devCount;i++){
    String arg_name(devTypeStr[0]);
    arg_name += String(i);
    arg_name.toCharArray(arg_name_buf,10);
    String v=server->arg(arg_name_buf);
    if(v.length() > 0){
      int value = 0;
      if( v == "on" ){
        value = 0xFF;
      }else
      if( v == "off" ){
        value = 0;
      }else
      if( v == "tgl" ){
        value = getDevValue(i) ? 0 : 0xFF;
      }else{
        value = v.toInt();
      }
      resp += '"';
      resp += arg_name;
      resp += "\": [";
      resp += getDevValue(i);
      resp += ",";
      resp += value;
      resp += "],";
      setDevValue(i, value);
      done--;
    }
  }
    //resp += (int)done;
  resp += "\"success\":true}";
  if(server->arg("noredirct").length()>0){
    server->send(200, contentType, resp);
  }else{
    server->send(302, "text/json\r\nLocation:/", resp);
  }
  yield();
  broadcastOff();
}
// WiFi async connector
void VVFsWebAPI::connectToWiFi() {
#ifndef __RECONNECT_WIFI_COMMENT
  if(millis()<nextSecondAt){return;}
  nextSecondAt = millis()+1000;
  if(WiFi.status() != WL_CONNECTED){
    if(millis() < wifiWaitTimeout){
      return;
    }
    wifiTry++;
    DEBUGLOG(wifiTry);
    if((wifiTry & 7) == 3){
      DEBUGLOG("scan networks");
      WiFi.disconnect();
      WiFi.scanNetworks();
    }
    if((wifiTry & 7) == 4){
      DEBUGLOG("WIFI:Init again");
      initWiFi();
    }

    if(wifiTry > 104){
      wifiTry = 0;
      DEBUGLOG("Restart device due to not connected to WiFi");
      ESP.restart();
    }
    return;
  }

  if(isWaitingWiFi){
    if(onWiFiConnect){
      onWiFiConnect();
    }

    DEBUGLOG("WIFI: Connected");
    DEBUGLOG(WiFi.localIP());

    delay(50);
    isWaitingWiFi = false;
  }
#endif
}

void VVFsWebAPI::initWiFi(){
  DEBUGLOG("Init WiFi begin");
  WiFi.begin(ssid, password);
  WiFi.mode(WIFI_STA);
  isWaitingWiFi = true;
  wifiWaitTimeout = millis()+20000;
  ::yield();
  DEBUGLOG("Init WiFi done");
}

void VVFsWebAPI::checkNotifyConnection(){
  if(!notifyConn || !notifyConn.connected()){
    notifyConn = notifySrv.available();
    if( notifyConn && notifyConn.connected()){
      broadcastOff();
    }
  }else{
    uint8_t inbuf[20];
    while(notifyConn.available()){
      notifyConn.read(inbuf, 20);
//      DEBUGLOG((char *)inbuf);
    }
  }
}

bool VVFsWebAPI::tcpNotify(const char * payload){
  if(!notifyConn || !notifyConn.connected()){
    return false;
  }
  notifyConn.write(payload);
  notifyConn.write("\r\n");
  notifyConn.flush();
  return true;
}
void VVFsWebAPI::anyNotify(const char * payload){
  if(!tcpNotify(payload)){
    udpNotify((char *)payload);
  }
}

int VVFsWebAPI::getAnalogValue(){
  return analogValue;
}
