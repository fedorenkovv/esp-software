#ifndef VVF_h
#define VVF_h
#include <WiFiUdp.h>
#include <ESP8266WebServer.h>

#ifdef DEBUG
#define DEBUGLOG(ln) Serial.println(ln)
#else
#define DEBUGLOG(ln) //ln
#endif
#ifndef CHECK_ANALOG_TIMEOUT
// twice per secont measure analog port
#define CHECK_ANALOG_TIMEOUT 500
#endif

typedef std::function<int(uint8_t devNo)> TGetDevValueCallback;
typedef std::function<void(uint8_t devNo, int newValue)> TSetDevValueCallback;

class VVFsWebAPI {
  ESP8266WebServer *server;
  unsigned long broadcastNext;
  uint16_t broadcastDelay;
  uint32_t requestRestartTime;
  uint8_t devCount;
  uint8_t buttonsCount;
  bool usedButtons;
  bool usedRelays;
  uint32_t nextAnalogMeasureTime;
  int analogValue;

  TSetDevValueCallback setDevValue;
  TGetDevValueCallback getDevValue;

  TGetDevValueCallback getButtonStatus;

  static WiFiUDP udp;
  static WiFiServer notifySrv;
  WiFiClient notifyConn;

  static const char* contentType;
  static const char* devTypeStr;

  // WiFi
  uint32_t wifiWaitTimeout;
  uint32_t nextSecondAt;
  uint8_t wifiTry;
  const char* ssid;
  const char* password;

  bool isWaitingWiFi;


  public:
    void (*onWiFiConnect)();
    VVFsWebAPI(const char*, const char*);
    void setup(ESP8266WebServer *, int);
    void yield();
    void setDeviceFn(TSetDevValueCallback,TGetDevValueCallback);
    void setupButtons(int8_t bc, TGetDevValueCallback _get);
    void broadcastOff();
    void udpNotify(char * payload);
    bool tcpNotify(const char * payload);
    void anyNotify(const char * payload);

    int getAnalogValue();

    void initWiFi();
    void connectToWiFi();

  protected:
    void send_i_am_here();
    void handle_reset();
    void handle_status();
    void handle_turn();

    void checkNotifyConnection();
    void measureAnalog();
};

#endif
