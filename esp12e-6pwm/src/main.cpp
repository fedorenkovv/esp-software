#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>
// #include <FS.h>
// #include <OneWire.h>

#include <VVF.h>
#include "Effects.h"

#ifdef ENABLE_OTA
#include <ESP8266HTTPUpdateServer.h>
#endif

#include "wifi_auth.h"
const char* ssid = WIFI_SSID;
const char* password = WIFI_PASSWORD;

ESP8266WebServer server(80);
#ifdef ENABLE_OTA
ESP8266HTTPUpdateServer httpUpdater;
#endif

uint8_t pwmPins[] = {5,4,10,13,16,0};
Effects e(pwmPins, 6);

VVFsWebAPI webapiServer(ssid, password);

void setPWMStatus(uint8_t devNo, int value){
  if(value>=MAXVALUE){
    if(e.getValue(devNo)<MAXVALUE){
      e.soft(devNo, 1);
    }
    return;
  }
  if(value<=0){
    if(e.getValue(devNo)>0){
     e.soft(devNo, -1);
    }
    return;
  }

  e.setValue(devNo, (uint16_t)value);
};

int getPWMStatus(uint8_t devNo){
  return (int)e.getValue(devNo);
}

void setup() {
  Serial.begin(115200);
  #ifdef DEBUG
  for(int i=0;i<15;i++){
    delay(1000);
    Serial.println(15-i);
  }
  Serial.println(" Start");
  #endif
  // 834539

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");


  webapiServer.setDeviceFn(setPWMStatus, getPWMStatus);
  webapiServer.setupButtons(0, [&](int8_t btnNo){ return 0;});
  webapiServer.setup(&server, 6);
  #ifdef ENABLE_OTA
  #ifdef DEBUG
  Serial.println("OTA setup");
  #endif
  httpUpdater.setup(&server);
  #endif

  server.on("/restart", [&]{
    if(server.method() == HTTP_POST){
      server.send(200, "text/html", "<h1>Restarting...</h1>");
      yield();
      ESP.restart();
      return;
    }
    server.send(200, "text/html", "<form method='post'><input type='submit' value='restart'></form>");
    yield();
  });
  server.on("/all-on", [&]{
    e.turnOnImmediate();
    server.send(200, "text/plain", "Ok");
    yield();
  });

  server.on("/all-off", [&]{
    e.turnOffImmediate();
    server.send(200, "text/plain", "Ok");
    yield();
  });

  server.on("/set-soft-speed", [&]{
    int16_t speed=400;
    String v=server.arg("speed");
    if(v.length()){
      speed = v.toInt();
    }
    e.setSoftSpeed(speed);
    server.send(200, "text/plain", "Ok");
    yield();
  });

  server.on("/effect", [&]{
    int8_t dirLights=1;
    int8_t dirPins=1;
    int16_t delayNext=65;
    int16_t speed=300;
    bool loop = server.arg("loop").length()>0;
    String v;
    String resp = "Ok,";
    v=server.arg("speed");
    if(v.length()){
      speed = v.toInt();
    }
    v=server.arg("dn");
    if(v.length()){
      delayNext = v.toInt();
    }
    v=server.arg("dp");
    if(v == "-"){
      dirPins =-1;
    }
    v=server.arg("dl");
    if(v == "-"){
      dirLights =-1;
    }
    resp += " dn:";
    resp += delayNext;
    resp += " dp:";
    resp += dirPins;
    resp += " dl:";
    resp += dirLights;
    e.startEffect(dirLights, dirPins, delayNext, speed, loop);
    server.send(200, "text/plain", resp);
    yield();
  });
  e.begin();
  server.begin();
}

void loop() {
  e.yield();
  webapiServer.yield();
  server.handleClient();
  yield();
}
