#include <pwm_effects.hpp>
  
uint16_t PWMEffects::valuesTranslate[100] =  {
      0, 4, 8, 12, 16, 32, 44, 60, 76, 88, 104, 120, 136, 152, 168, 188, 204, 220, 240, 256, 276, 296, 316, 336, 356, 376, 400, 420, 444, 468, 492, 512, 540, 564, 588, 616, 640, 668, 696, 724, 752, 784, 812, 844, 876, 908, 940, 976, 1008, 1044, 1080, 1116, 1152, 1192, 1232, 1272, 1312, 1352, 1396, 1440, 1484, 1528, 1576, 1624, 1672, 1720, 1772, 1820, 1876, 1928, 1984, 2040, 2096, 2156, 2212, 2276, 2336, 2400, 2464, 2532, 2600, 2668, 2740, 2812, 2888, 2960, 3040, 3116, 3196, 3280, 3364, 3448, 3536, 3628, 3720, 3812, 3908, 4004, 4090, 4093
  };

int8_t PWMEffects::translateBackValue(uint16_t v){
  int top = 100;
  int bottom = 0;
  int idx;
  for(int i=0;i<9;i++){
    idx = (top + bottom) / 2;
    if(valuesTranslate[idx] == v || idx == top || idx == bottom){
      return idx;
    }
    if(valuesTranslate[idx] < v){
      bottom = idx;
    }else{
      top = idx;
    }
  }
  return idx;
}

uint8_t PWMEffects::getPinValue(uint8_t pin) { 
  if(pinPercent[pin]<0){
    uint16_t pinValue = pwmDriver->getChannelPWM(pin);
    pinPercent[pin] = translateBackValue(pinValue);
  }
  return pinPercent[pin];
}

void PWMEffects::startEffect(uint16_t stepTime, uint8_t startPin, uint8_t endPin,
                             uint16_t lightDestination,
                             uint16_t nextPinStartAfterStepsCount) {
  this->stepTime = stepTime;
  if(stepTime == 0){
    this->stepTime = 1;
  }
  this->startPin = startPin;
  this->endPin = endPin;
  if(lightDestination > 100){
    lightDestination = 100;
  }
  this->lightDestination = lightDestination;
  this->nextPinStartAfterStepsCount = nextPinStartAfterStepsCount;
  lightDirection = getPinValue(startPin) > lightDestination ? -1 : 1;
  pinsDirection = startPin > endPin ? -1 : 1;
  currentPin = startPin;
  stepsDone = 0;
  isDoing = true;
  timeToNextStep = 0;
  for(int i=0;i<16;i++){pinPercent[i] = -1;}
  yield();  
}

void PWMEffects::stop() { 
    isDoing = false; 
    stepsDone = 0;
    stepTime = 0;
    for(int i=0;i<16;i++){pinPercent[i] = -1;}
}

void PWMEffects::yield() {
  if (!isDoing) {
    return;
  }
  long long timeNow = millis();
  if (timeToNextStep > timeNow) {
    return;
  }
  timeToNextStep = timeNow + (long long)this->stepTime;
  doStep();
}

void PWMEffects::doStep() {
  stepsDone++;
  if (stepsDone >= nextPinStartAfterStepsCount) {
    stepsDone = 0;
    if (currentPin != endPin) {
      currentPin += pinsDirection;
    }
  }
  uint8_t fromPin = startPin;
  uint8_t toPin = currentPin;
  if (fromPin > toPin) {
    toPin = startPin;
    fromPin = currentPin;
  }
  bool hasChanges = false;
  for (int8_t pin = fromPin; pin <= toPin; pin++) {
    uint8_t pinValue = getPinValue(pin);
  #ifdef TEST
  printf(" %d:%03X ", pin, pinValue);
  #endif
    int lightDirection = pinValue >= lightDestination ? -1 : 1;
    if ((lightDirection > 0 && pinValue >= lightDestination) ||
        (lightDirection < 0 && pinValue <= lightDestination)) {
#ifdef TEST
      printf("SkipPin: lightDirection=%d, %04X %s %04X = %d\t",
      lightDirection, pinValue,
      lightDirection > 0 ? ">=" : "<=",
      lightDestination,
      lightDirection > 0 ? pinValue >= lightDestination
                           : pinValue <= lightDestination
      );
#endif
      continue;
    }
    setPinValue(pin, pinValue + lightDirection);
    hasChanges = true;

    ::yield();
  }
  isDoing = hasChanges || currentPin != endPin;
}

void PWMEffects::setPinValue(uint8_t pin, uint8_t percent){
  pinPercent[pin] = percent;
  if (percent >= 100) {
    #ifdef TEST
    printf(" %d:ON ", pin);
    #endif
    pwmDriver->setChannelOn(pin);
    return;
  } 
  if (percent == 0) {
    #ifdef TEST
    printf(" %d:OFF ", pin);
    #endif
    pwmDriver->setChannelOff(pin);
    return;
  }
  #ifdef TEST
  printf(" %d=%03X (%d%%)", pin, valuesTranslate[percent], percent);
  #endif
  pwmDriver->setChannelPWM(pin, valuesTranslate[percent]);
}
