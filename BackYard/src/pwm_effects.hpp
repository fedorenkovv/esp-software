#ifndef PWM_EFFECTS_HPP
#define PWM_EFFECTS_HPP

#include "tests.h"
#include <PCA9685.h>

class PWMEffects {
#ifdef TEST
public:
#else
private:
#endif
  PCA9685 *pwmDriver;
  long long timeToNextStep;
  long long stepTime;
  uint8_t startPin;
  uint8_t endPin;
  uint8_t currentPin;
  int16_t lightDirection;
  int8_t pinsDirection;
  uint16_t lightDestination;
  uint16_t nextPinStartAfterStepsCount;
  uint16_t stepsDone;
  int8_t pinPercent[16];
  
  static uint16_t valuesTranslate[100];
  
  bool isDoing;
  void doStep();

public:
  PWMEffects(PCA9685 *pwmDriver)
      : pwmDriver(pwmDriver), isDoing(false), timeToNextStep(0){};
  void startEffect(uint16_t stepTime, uint8_t startPin, uint8_t endPin,
                   uint16_t lightDestination,
                   uint16_t nextPinStartAfterStepsCount);
  void yield();
  void stop();
  static int8_t translateBackValue(uint16_t v);
#define EFFECT_FOR_FIVE_STEP_TIME 10
#define EFFECT_FOR_FIVE_NEXT_PIN_STEPS 80
  void doEffectOnFromStartFive() {  startEffect(EFFECT_FOR_FIVE_STEP_TIME, 0, 4, 100, EFFECT_FOR_FIVE_NEXT_PIN_STEPS); }
  void doEffectOffFromStartFive() { startEffect(EFFECT_FOR_FIVE_STEP_TIME, 0, 4, 0,   EFFECT_FOR_FIVE_NEXT_PIN_STEPS); }
  void doEffectOnFromEndFive() {    startEffect(EFFECT_FOR_FIVE_STEP_TIME, 4, 0, 100, EFFECT_FOR_FIVE_NEXT_PIN_STEPS); }
  void doEffectOffFromEndFive() {   startEffect(EFFECT_FOR_FIVE_STEP_TIME, 4, 0, 0,   EFFECT_FOR_FIVE_NEXT_PIN_STEPS); }
  void doOnePinUpTo(uint8_t pin, uint16_t value) {
    startEffect(5, pin, pin, value, 1);
  }
  void doOnePinOn(uint8_t pin) { doOnePinUpTo(pin, 100); }
  void doOnePinOff(uint8_t pin) { doOnePinUpTo(pin, 0); }

  void setPinValue(uint8_t pin, uint8_t percent);
  uint8_t getPinValue(uint8_t pin);
};
#endif