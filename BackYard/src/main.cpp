#include <Arduino.h>
#include <PCA9685.h>
#include <PCF857X.h>
#include <Wire.h>

#define DEBUG
#include <VVF.h>
#include <pwm_effects.hpp>

#include "wifi_auth.h"

#ifndef ENABLE_OTA
#define ENABLE_OTA
#endif

#ifdef ENABLE_OTA
#include <ESP8266HTTPUpdateServer.h>
#endif

const char *ssid = WIFI_SSID;
const char *password = WIFI_PASSWORD;

ESP8266WebServer server(80);
#ifdef ENABLE_OTA
ESP8266HTTPUpdateServer httpUpdater;
#endif

VVFsWebAPI webapiServer(ssid, password);
#define BUTTONS_COUNT 8

#define BTN0_PIN 12
#define BTN1_PIN 13

#define PWMS_COUNT 16

#define DEBOUNCE_TIMEOUT 180

// volatile unsigned long timeToNextToggle = 0;

volatile bool buttonStates[BUTTONS_COUNT];
volatile bool buttonStateChanged[BUTTONS_COUNT];
volatile unsigned long buttonDebounceTimeouts[BUTTONS_COUNT];
volatile bool haveToCheckButtons = false;

PCA9685 pwmDriver; // = PCA9685(0x70, PCA9685_MODE_N_DRIVER, 500.0);
PCF857X expander = PCF857X();
uint8_t inputsExpanderAddress = 0x21;
#define PCF8575_INT_PIN 14

PWMEffects effects(&pwmDriver);

ICACHE_RAM_ATTR
void buttonsInterruptHandler() {
  haveToCheckButtons = true;
  // Serial.println("\nbutton changes");
}

void updateButtons() {
  uint16_t allButtonsState = expander.read() << 2;
  if(digitalRead(BTN0_PIN)){
    allButtonsState |= 1;
  }else{
    allButtonsState &= 0xFE;
  }
  if(digitalRead(BTN1_PIN)){
    allButtonsState |= 2;
  }else{
    allButtonsState &= 253;
  }
  for (int i = 0; i < BUTTONS_COUNT; i++) {
    bool newState = (allButtonsState & (1 << i)) != 0;
    if (buttonDebounceTimeouts[i] && millis() > buttonDebounceTimeouts[i]) {
      buttonDebounceTimeouts[i] = 0;
      if (newState != buttonStates[i]) {
        // fire event:
        buttonStates[i] = newState;
        buttonStateChanged[i] = true;
        // Serial.printf("Btn %d - %d\n", i, newState);
      }
      continue;
    }
    if (!buttonDebounceTimeouts[i] && newState != buttonStates[i]) {
      buttonDebounceTimeouts[i] = millis() + DEBOUNCE_TIMEOUT;
    }
  }
}

void reactButtons() {
  if (buttonStateChanged[0]) {
    if (buttonStates[0]) {
      effects.doEffectOnFromStartFive();
    } else {
      effects.doEffectOffFromEndFive();
    }
  }
  if (buttonStateChanged[1]) {
    if (buttonStates[1]) {
      effects.startEffect(3, 5, 9, 100, 50);
    } else {
      effects.startEffect(3, 5, 9, 0, 50);
    }
  }
  for(int i=2;i<8;i++){
    if(buttonStateChanged[i]){
      if(buttonStates[i]){
        effects.startEffect(1, i, i, 100, 100);
      } else {
        effects.startEffect(1, i, i, 0, 100);
      }
    }
  }
  for(int i=0;i<8;i++){
    if(buttonStateChanged[i]){
      String eventStr= (buttonStates[i] ? "up" : "down");
      eventStr += ":" + String(buttonStates[i], DEC) + ":" + i;
      if(!webapiServer.tcpNotify(eventStr.c_str())){
        webapiServer.udpNotify((char *)eventStr.c_str());
      }
    }
    buttonStateChanged[i] = false;
  }
}

void scanI2C(ESP8266WebServer *server = NULL) {
  byte error, address;
  int nDevices;
  int nIOExps = 0;
  nDevices = 0;
  String result = "{ \"scanned\":1";
  for (address = 8; address < 127; address++) {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0) {
      Serial.print("I2C device found at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.print(address, HEX);
      Serial.println(" !");
      if ((address & 0xF8) == 0x20) {
        inputsExpanderAddress = address;
        nIOExps ++;
      }

      nDevices++;
      if (server) {
        result += ", \"dev";
        result += String(address, HEX);
        result += "\": ";
        result += address;
      }
    } else if (error == 4) {
      Serial.print("Unknow error at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.println(address, HEX);
      if (server) {
        result += ", \"dev";
        result += String(address, HEX);
        result += "\": [";
        result += address;
        result += ", \"Error\"]";
      }
    }
  }
  Serial.println(nDevices == 0 ? "No I2C devices found\n" : "done\n");
  if (server) {
    result += ", \"count\":";
    result += nDevices;
    result += ", \"gpioExpCount\":";
    result += nIOExps;
    result += "}";
    server->send(200, "text/json", result);
  }
}

int parseArgInt(const char * argName, int defaultValue){
  String value = server.arg(argName);
  if(value.length() == 0){
    return defaultValue;
  }
  return value.toInt();
}

void setup() {
  Serial.begin(9600);
#ifdef DEBUG
  delay(3000);
#endif
  Wire.begin();
  // Wire.setClock(300000);
  pinMode(0, INPUT_PULLUP);
  pinMode(BTN0_PIN, INPUT_PULLUP);
  pinMode(BTN1_PIN, INPUT_PULLUP);
  pinMode(16, INPUT_PULLDOWN_16);
  // This pin is pull down only if chip has been just programmed by serial.
  if (!digitalRead(0)) {
    Serial.setDebugOutput(true);
  }
  /*
  pwmDriver.resetDevices(); // Software resets all PCA9685 devices on Wire line
  pwmDriver.init(
      0,
      5); // Address pins A5-A0 = 0, mode = use driver (0x04), no invert (0x10)
  pwmDriver.setPWMFrequency(120);
  */
  scanI2C();
  
  // attachInterrupt(PCF8575_INT_PIN, buttonsInterruptHandler, FALLING);
  expander.begin(inputsExpanderAddress, CHIP_PCF8574);
  for(int pin=0;pin < BUTTONS_COUNT; pin ++){
    expander.pinMode(pin, INPUT_PULLUP);
    buttonDebounceTimeouts[pin] = millis();
    buttonStates[pin] = true;
  }
  uint16_t relayStates[PWMS_COUNT];
  for (int i = 0; i < PWMS_COUNT; i++) {
    relayStates[i] = 0;
  }
  pwmDriver.setChannelsPWM(0, PWMS_COUNT, relayStates);
  WiFi.begin(ssid, password);
  WiFi.mode(WIFI_STA);

  webapiServer.setup(&server, PCA9685_CHANNEL_COUNT);
  webapiServer.setupButtons(BUTTONS_COUNT, [&](int8_t btnNo) {
    // return expander.digitalRead(btnNo);
    return buttonStates[btnNo];
  });
  server.on("/scanI2C", [&] { scanI2C(&server); });
  server.on("/light-on", [&] {    
    uint8_t s = parseArgInt("kspeed", 3);
    uint8_t d = parseArgInt("delay", 33);
    effects.startEffect(s, 0, 4, 100, d);
    server.send(200, "text/json", "{\"Ok\":true}");
  });
  server.on("/light-off", [&] {
    uint8_t s = parseArgInt("kspeed", 3);
    uint8_t d = parseArgInt("delay", 33);
    effects.startEffect(s, 0, 4, 0, d);
    server.send(200, "text/json", "{\"Ok\":true}");
  });
  server.on("/fast-fine-turn", [&] {
    effects.stop();
    for (int i = 0; i < 16; i++) {
      String argName = "d";
      argName += String(i);
      String value = server.arg(argName); 
      if (value.length() <= 0) {
        continue;
      }
      if (value == "off") {
        pwmDriver.setChannelOff(i);
      } else if (value == "on") {
        pwmDriver.setChannelOn(i);
      } else {
        int16_t newValue = value.toInt();
        pwmDriver.setChannelPWM(i, newValue);
      }
    }
    server.send(200, "text/json", "{\"Ok\":true}");
  });
  server.on("/effect", [&] {
    effects.stop();
    uint8_t stepTime = parseArgInt("stepTime", 3);
    uint8_t startPin = parseArgInt("fromPin", 0);
    uint8_t endPin = parseArgInt("toPin", 5);
    uint8_t lightDestination = parseArgInt("level", 100);
    uint8_t nextPinStartAfterStepsCount = parseArgInt("whenNextPin", lightDestination-1);
    if (!stepTime || !nextPinStartAfterStepsCount) {
      server.send(200, "text/json", "{\"Ok\":false}");
      return;
    }
    effects.startEffect(stepTime, startPin, endPin, lightDestination,
                        nextPinStartAfterStepsCount);
    server.send(200, "text/json", "{\"Ok\":true}");
  });
  
  server.on("/read-ex-pin", [&] {
    int pinNo = parseArgInt("pin", 20) & 0xFF;
    if(pinNo == 20){
      String resp = "{\"hex\":\"";
      resp += String(expander.read(), HEX);
      resp += "\", \"value\":";
      resp += String(expander.read(), DEC);
      resp += "}";
      server.send(200, "text/json", resp);
      return;
    }
    String resp = "{\"pin";
    resp += String(pinNo, DEC);
    resp += "\":\"";
    resp += expander.digitalRead(pinNo) ? "on" : "off";
    resp += "\"}";
    server.send(200, "text/json", resp);
  });

  webapiServer.setDeviceFn(
      [&](int8_t devNo, int newValue) {
        effects.stop();
        effects.setPinValue(devNo, newValue);
      },
      [&](int8_t devNo) { return (int)effects.getPinValue(devNo); });

#ifdef ENABLE_OTA
  httpUpdater.setup(&server);
#endif
  server.begin();
}

void loop() {
  webapiServer.yield();
  yield();
  server.handleClient();
  yield();
  // if (haveToCheckButtons) {
  //   haveToCheckButtons = false;
  //   Serial.println("Buttons interrupt was called. check buttons states");
  // }
  updateButtons();
  yield();
  reactButtons();
  yield();
  effects.yield();
}