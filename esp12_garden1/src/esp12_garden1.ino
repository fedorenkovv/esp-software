#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <JC_Button.h>

#include <VVF.h>
#include "wifi_auth.h"

#ifdef ENABLE_OTA
#include <ESP8266HTTPUpdateServer.h>
#endif


const char* ssid = WIFI_SSID;
const char* password = WIFI_PASSWORD;

ESP8266WebServer server(80);
#ifdef ENABLE_OTA
ESP8266HTTPUpdateServer httpUpdater;
#endif

#define VALVE_COUNT 2
#define STATUS_OFF 0
#define STATUS_ON 1
#define STATUS_GETTING_OFF 2
#define STATUS_GETTING_ON 3

uint8_t valvePins[][2] = {{12,13},{16,14}};
uint8_t statuses[] = {0,0};
uint32_t wait_loops[] = {0,0};
uint32_t _ms = millis();

#define DEBOUNCE_MS 20
#define INIT_BUTTON(pin) Button(pin, true, false, DEBOUNCE_MS)
Button buttons[]={
  INIT_BUTTON(4),
  INIT_BUTTON(5)
};
bool enableButtons = false;

VVFsWebAPI webapiServer(ssid, password);

void setStatus(uint8_t devNo, int value){
  DEBUGLOG(String("setStatus ") + devNo+" "+ value);
  if(value==33){
    digitalWrite(valvePins[devNo][0], 1);
    digitalWrite(valvePins[devNo][1], 1);
    wait_loops[devNo] = millis() + 2000;
    statuses[devNo] |= 7;
  }
  // value = value ? 1 : 0;
  // if((statuses[devNo] & 1) ^ value){
  //   return;
  // }
  if(value){
    statuses[devNo] = STATUS_GETTING_ON;
    // turn power on +-
    digitalWrite(valvePins[devNo][0], 0);
    digitalWrite(valvePins[devNo][1], 1);
  }else{
    statuses[devNo] = STATUS_GETTING_OFF;
    // turn power on -+
    digitalWrite(valvePins[devNo][0], 1);
    digitalWrite(valvePins[devNo][1], 0);
  }
  wait_loops[devNo] = millis()+250;
};

int getStatus(uint8_t devNo){
  DEBUGLOG("get status");
  DEBUGLOG(statuses[devNo]);
  return (int)statuses[devNo];
}


void setup(){

  Serial.begin(115200);

  pinMode(valvePins[0][0], OUTPUT);
  pinMode(valvePins[0][1], OUTPUT);
  pinMode(valvePins[1][0], OUTPUT);
  pinMode(valvePins[1][1], OUTPUT);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);

  #ifdef DEBUG
  delay(5000);
  Serial.println("Start");
  #endif

  // Serial.println("Effects start effect");
  webapiServer.setup(&server, 2);
  webapiServer.setDeviceFn(setStatus, getStatus);
  webapiServer.setupButtons(2, [&](int8_t btnNo){
    if(btnNo>=2){ return 42; }
    return buttons[btnNo].isPressed() ? 1 : 0;
  });

  #ifdef ENABLE_OTA
  httpUpdater.setup(&server);
  #endif
// /*

  #ifdef DEBUG
  Serial.println("init handlers");
  #endif
  server.on("/isGarden1", [&]{
    server.send(200, "text/plain", "Yes:3");
    yield();
  });
  server.on("/enableButtons", [&]{
    enableButtons = true;
    server.send(200, "text/plain", "Ok");
    yield();
  });
  server.on("/disableButtons", [&]{
    enableButtons = false;
    server.send(200, "text/plain", "Ok");
    yield();
  });
  server.on("/restart", [&]{
    if(server.method() == HTTP_POST){
      server.send(200, "text/html", "<h1>Restarting...</h1>");
      yield();
      ESP.restart();
      return;
    }
    server.send(200, "text/html", "<form method='post'><input type='submit' value='restart'></form>");
    yield();
  });

  // */
  yield();
  server.begin();
}
void checkValveOff(){
  uint32_t _ms = millis();
  for(int8_t i =0;i<VALVE_COUNT; i++){
    if(wait_loops[i] && _ms > wait_loops[i]){
      digitalWrite(valvePins[i][0], 0);
      digitalWrite(valvePins[i][1], 0);
      wait_loops[i] = 0; // diable checking
      statuses[i] &= 1; // remove "-ing" flag, doing->done
      String event = "valve:done:";
      event += (i+1);
      event += ":";
      event += statuses[i];
      webapiServer.anyNotify(event.c_str());
    }
  }
}

void checkButtons(){
  for(size_t i=0;i<2;i++){
    buttons[i].read();
    bool hasEvent = false;
    String event = "button:";
    if(buttons[i].wasPressed()){
      event += "press:";
      hasEvent = true;
    }else if(buttons[i].wasReleased()){
      event += "release:";
      hasEvent = true;
    }
    if(hasEvent){
      event += (int)(i+1);
      webapiServer.anyNotify(event.c_str());
    }
  }
}
#define ANALOG_HIGH 1000
#define ANALOG_LOW 600
uint32_t analogCheckTime = 0;
bool isAnalogHigh = false;
void checkAnalog(){
  uint32_t _ms = millis();
  if(analogCheckTime < _ms){
    int analogValue = webapiServer.getAnalogValue();
    if((isAnalogHigh && analogValue < ANALOG_LOW) || (!isAnalogHigh && analogValue > ANALOG_HIGH)){
      isAnalogHigh = !isAnalogHigh;
      analogCheckTime = _ms + 10000;
      String payload = "analog:";
      payload += isAnalogHigh ? "up" : "down";
      payload += ":value=";
      payload += analogValue;
      webapiServer.anyNotify(payload.c_str());
    }
  }
}
void loop(){
  webapiServer.yield();
  server.handleClient();
  yield();
  checkValveOff();
  checkButtons();
  checkAnalog();
  delay(1);
}
