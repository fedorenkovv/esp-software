#include <Arduino.h>
#include <VVF.h>

#include "wifi_auth.h"
#define ENABLE_OTA 

#ifdef ENABLE_OTA
#include <ESP8266HTTPUpdateServer.h>
#endif

const char* ssid = WIFI_SSID;
const char* password = WIFI_PASSWORD;

ESP8266WebServer server(80);
#ifdef ENABLE_OTA
ESP8266HTTPUpdateServer httpUpdater;
#endif

VVFsWebAPI webapiServer(ssid, password);
#define BUTTON_PIN 12
uint8_t relayState = 0;
unsigned long timeToNextToggle = 0;
unsigned long reCheckChangesTime = 0;
bool buttonState = false;
bool buttonStateChanged = false;

void relay_on(uint8_t relayNo){
  relayState = 255;
  digitalWrite(4, !relayState);
  digitalWrite(5, !relayState);
}
void relay_off(uint8_t relayNo){
  relayState = 0;
  digitalWrite(4, !relayState);
  digitalWrite(5, !relayState);
}

void updateButton(){
  bool newState = digitalRead(BUTTON_PIN);
  if(reCheckChangesTime && millis() > reCheckChangesTime){
    reCheckChangesTime = 0;
    if(newState != buttonState){
      // fire event:
      buttonState = newState;
      buttonStateChanged = true;
    }
    return;
  }
  if(!reCheckChangesTime && newState != buttonState){
    reCheckChangesTime = millis() + 180;
  }
}

void setup(){
  Serial.begin(9600);
  #ifdef DEBUG
    delay(3000);
  #endif
  pinMode(0, INPUT_PULLUP);
  // This pin is pull down only if chip has been just programmed by serial.
  if(!digitalRead(0)){
    Serial.setDebugOutput(true);
  }

  WiFi.begin(ssid, password);
  WiFi.mode(WIFI_STA);

  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(BUTTON_PIN, INPUT_PULLUP);

  webapiServer.setup(&server, 2);
  webapiServer.setupButtons(1, [&](int8_t btnNo){
    return buttonState;
  });
  
  webapiServer.setDeviceFn([&](int8_t relayNo, int newValue){
    if(newValue){
        relay_on(relayNo);
    }else{
        relay_off(relayNo);
    };
  }, [&](int8_t relayNo){
      return (int)relayState;
  });

  #ifdef ENABLE_OTA
  httpUpdater.setup(&server);
  #endif
  server.begin();

}

void loop() {
  webapiServer.yield();
  yield();
  server.handleClient();
  yield();
  updateButton();
  yield();
  if(!buttonStateChanged){
    return;
  }
  yield();
  buttonStateChanged = false;
  const char * btnNotifyMessage = buttonState ? "up:1:0" : "down:2:0";
  if(!webapiServer.tcpNotify(btnNotifyMessage)){
    webapiServer.udpNotify((char *)btnNotifyMessage);
  }
  if(buttonState && timeToNextToggle < millis()){
    timeToNextToggle = millis() + 1000;
    if(relayState){
      relay_off(0);
    }else{
      relay_on(0);
    }
  }
}