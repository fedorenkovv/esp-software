#include <Arduino.h>
#include <OneWire.h>
#include <DallasTemperature.h>
// #define DEBUG
#include <VVF.h>
#include <ESP8266FtpServer.h>
#include <FS.h>
#include <ESP8266HTTPClient.h>
#include "wifi_auth.h"

#define ENABLE_OTA 

#ifdef ENABLE_OTA
#include <ESP8266HTTPUpdateServer.h>
#endif

// #define ENABLE_TEMP

#ifdef ENABLE_TEMP
OneWire oneWire(14);
DallasTemperature sensors(&oneWire);
#ifdef DEBUG
unsigned long periodTemperatureAsk = 30000; // 30 sec
#else
unsigned long periodTemperatureAsk = 300000; // 5 min
#endif
unsigned long nextTemeratureAsk=0, whenTemperatureReady=0;
DeviceAddress thermometerAddress;

#endif 
FtpServer ftpSrv;

const char* ssid = WIFI_SSID;
const char* password = WIFI_PASSWORD;

#define EVENT_COUNT 2

uint16_t debounce=200;
enum TButtonEvents {EVENT_DOWN, EVENT_UP} ;
const char* eventNames[]={
  "up", "down"
};

ESP8266WebServer server(80);
#ifdef ENABLE_OTA
ESP8266HTTPUpdateServer httpUpdater;
#endif

VVFsWebAPI webapiServer(ssid, password);

const uint8_t relay_pins[]={4,5};

bool relay_states[]={0,0};
#define applyPin(i) digitalWrite(relay_pins[i], !relay_states[i])

typedef void (*TEventCallback)(uint8_t i);

void relay_toggle(uint8_t i){
  relay_states[i] = !relay_states[i];
  applyPin(i);
  #ifdef DEBUG
    Serial.print("tgl ");
    Serial.println(i);
  #endif
}

void relay_off(uint8_t i){
    relay_states[i] = 0;
    applyPin(i);
    #ifdef DEBUG
        Serial.print("off ");
        Serial.println(i);
    #endif
}
void relay_on(uint8_t i){
    relay_states[i] = 1;
    applyPin(i);
    #ifdef DEBUG
        Serial.print("on ");
        Serial.println(i);
    #endif
}

//void no_op(uint8_t i){}

void relay_all_off(uint8_t i){
  for(i=0;i<2;i++) relay_off(i);
}
void relay_all_on(uint8_t i){
  for(i=0;i<2;i++) relay_on(i);
}

void relay_all_toggle(uint8_t i){
  TEventCallback act = relay_states[i] ? relay_off : relay_on;
  for(i=0;i<2;i++) act(i);
}

const TEventCallback possibleHandlers[] =
  { NULL, relay_toggle, relay_on, relay_off, relay_all_toggle, relay_all_on, relay_all_off };

const String hanlersNames[] = {
    "NULL", "TOGGLE", "ON", "OFF", "ALL_TOGGLE", "ALL_ON", "ALL_OFF"
};
const char * defaultReaction[4][2] = {
    {"ON0", "OFF0"},
    {"ON1", "OFF1"},
    {"disabled", "disabled"},
    {"disabled", "disabled"},
};

// enum THandlerCode { NO_OP, RELAY_TOGGLE, RELAY_ON, RELAY_OFF, RELAY_ALL_TOGGLE, RELAY_ALL_ON, RELAY_ALL_OFF };
#define NO_OP 0
#define RELAY_TOGGLE 1
#define RELAY_ON 2
#define RELAY_OFF 3
#define RELAY_ALL_TOGGLE 4
#define RELAY_ALL_ON 5
#define RELAY_ALL_OFF 6

struct Button{
  unsigned long reCheckChangesTime;
  bool state;
  const int8_t pin;
  int8_t relayNo;
  char * eventReactions[EVENT_COUNT];
  TEventCallback reactionCallbacks[EVENT_COUNT];

  Button(int8_t p, int8_t npp): pin(p), reCheckChangesTime(0), relayNo(npp){
    for(int i=0;i<EVENT_COUNT; i++){
      eventReactions[i] = NULL;
      reactionCallbacks[i] = NULL;
    }
  }

  void begin(){
    pinMode(pin, pin==16 ? INPUT : INPUT_PULLUP);
    state = digitalRead(pin);
  }

  void setEventReaction(int8_t eventNo, char* reaction){
    eventReactions[eventNo] = reaction;
    String strReaction = reaction;
    if(strReaction.startsWith("URL:") || String(reaction) == "disabled"){
        return;
    }
    for(uint16_t i=0; i<sizeof(hanlersNames); i++){
        if(strReaction.startsWith(hanlersNames[i])){
            reactionCallbacks[eventNo] = possibleHandlers[i];
            if(strReaction.length() > hanlersNames[i].length() ){
                relayNo = (int8_t)strReaction.substring(hanlersNames[i].length()).toInt();
            }
            break;        
        }
    }
  }  

  void fireChanged(int8_t btnNo){
    int eventNo = state ? EVENT_UP : EVENT_DOWN;
    if(reactionCallbacks[eventNo]){
        reactionCallbacks[eventNo](relayNo);
        sendEvent(btnNo, eventNo, "");
        return;
    }
    String reaction(eventReactions[eventNo]);
    String httpSuccess = "";
    if(!reaction || reaction == "disabled"){
        sendEvent(btnNo, eventNo, httpSuccess);
        return; // don't sent to server
      }

    if(reaction.length()>4 && reaction.startsWith("URL:")){
        HTTPClient http;
        WiFiClient wifiClient;
        http.begin(wifiClient, reaction.substring(4));
        int httpCode = http.GET();
        if(httpCode == HTTP_CODE_OK) {
            String payload = http.getString();
            httpSuccess = "http=ok,";
            httpSuccess += payload.length();
            for(int i=0;i < 1e5 && http.connected(); i++){
                delay(10);
                ::yield();
            }
        } else {
            httpSuccess = "http=error,";
            httpSuccess += httpCode;
        }
        http.end();
    }
    ::yield();
    sendEvent(btnNo, eventNo, httpSuccess);
  }

  void sendEvent(int8_t btnNo, int eventNo, String httpSuccess){
    String payload=eventNames[eventNo];
    payload += ":";
    payload += String(1+eventNo, DEC);
    payload += ":";
    payload += btnNo;
    if(httpSuccess){
      payload += ":";
      payload += httpSuccess;
    }
    char buf[payload.length()+1];
    payload.toCharArray(buf, payload.length()+1);
    if(!webapiServer.tcpNotify(buf)){
      webapiServer.udpNotify(buf);
    };
  }

  void read(int8_t btnNo){
    bool newState = digitalRead(pin);
    if(reCheckChangesTime && millis() > reCheckChangesTime){
      reCheckChangesTime = 0;
      if(newState != state){
        // fire event:
        state = newState;
        fireChanged(btnNo);
      }
      return;
    }
    if(!reCheckChangesTime && newState != state){
      reCheckChangesTime = millis() + debounce;
    }
  }
};

#define BUTTONS_COUNT 4

Button buttons[]{
  Button(12, 0), Button(13, 1), Button(16, 0), Button(14, 1)
};

const char* content_type="text/json";

void setup(){
  Serial.begin(9600);
  #ifdef DEBUG
    delay(3000);
  #endif
  pinMode(0, INPUT_PULLUP);
  // This pin is pull down only if chip has been just programmed by serial.
  if(!digitalRead(0)){
    Serial.setDebugOutput(true);
  }  

#ifdef ENABLE_TEMP
  sensors.begin();
  sensors.setWaitForConversion(false);
  nextTemeratureAsk = millis() + 10000;
  whenTemperatureReady = 0;
  // TODO: setResolution for all sensors

#endif

  WiFi.begin(ssid, password);
  WiFi.mode(WIFI_STA);

  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  relay_all_off(0);

  webapiServer.setup(&server, 2);
  webapiServer.setupButtons(BUTTONS_COUNT, [&](int8_t btnNo){
    return buttons[btnNo].state;
  });
  
  webapiServer.setDeviceFn([&](int8_t relayNo, int newValue){
    if(newValue){
        relay_on(relayNo);
    }else{
        relay_off(relayNo);
    };
  }, [&](int8_t relayNo){
      return (int)relay_states[relayNo];
  });
  

  SPIFFS.begin();
  #ifdef DEBUG
  /*
  server.on("/format-fs",[&]{
    if(SPIFFS.exists("/.lock-fmt")){
      server.send(200, content_type, "{ \"success\":false, \"alreadyFormatted\":true }");
      delay(2);
      return;
    }
    if(SPIFFS.format()){
      File f = SPIFFS.open("/.lock-fmt", "w");
      f.write('V');
      f.close();
      server.send(200, content_type, "{ \"success\":true }");
    }else{
      server.send(200, content_type, "{ \"success\":false }");
    }
    yield();
  });
*/
  #endif

#ifdef ENABLE_TEMP
  server.on("/temp", [&]{
    int countSensors = sensors.getDeviceCount();
    if(countSensors<=0){
      sensors.begin();
    }
    String resp = "{\"n\":";
    resp += countSensors;
    if(whenTemperatureReady){
      resp += ",\"error\":\"measure\"";
    }else{
      resp += ", \"t\":[";
      for(int i=0;i<countSensors;i++){
        if(!sensors.getAddress(thermometerAddress, i)){
          continue;
        }
        resp += "{\"address\":\"";
        for(int j=0;j<8;j++){
          resp += String(thermometerAddress[j], HEX);
        }
        resp += "\", \"value\":";
        float temperature = sensors.getTempC(thermometerAddress);
        resp += String(temperature);
        resp += "}";
        if( i ){
          resp += ", ";
        }
      }
      resp += "]";
      nextTemeratureAsk = 1;
    }
    resp += "}";
    server.send(200, content_type, resp);
    yield();
  });
#endif

  ftpSrv.begin("vvf", "vvf");

  #ifdef ENABLE_OTA
  httpUpdater.setup(&server);
  #endif

  server.begin();
  // Start buttons and Init event reactions
  DEBUGLOG("Start buttons");
  for(int8_t i=0; i<BUTTONS_COUNT; i++){
    buttons[i].begin();
    DEBUGLOG("Button:" + i);
    for(int8_t eventNo=0;eventNo < EVENT_COUNT; eventNo++){
      String fileName = "/";
      fileName += eventNames[eventNo];
      DEBUGLOG("   event:" + fileName);
      fileName += i;
      fileName += ".txt";
      if(SPIFFS.exists(fileName)){
        File f = SPIFFS.open(fileName, "r");
        if(f){
            size_t fileSize = f.size();
            char * reaction = new char[fileSize+1];
            reaction[fileSize] = 0;
            f.readBytes(reaction, fileSize);
            buttons[i].setEventReaction(eventNo, reaction);
            DEBUGLOG("readed reaction:");
            DEBUGLOG(reaction);
            f.close();
        }
      } else {
        DEBUGLOG("   set default reaction: ");
        DEBUGLOG(defaultReaction[i][eventNo]);
        buttons[i].setEventReaction(eventNo, (char *)defaultReaction[i][eventNo]);
      }
    }
  }
}

#ifdef ENABLE_TEMP
void handleThermometers(){
  if(whenTemperatureReady && millis() > whenTemperatureReady){
    whenTemperatureReady = 0;
    int countSensors = sensors.getDeviceCount();
    String payload = "temp:n=";
    payload += countSensors;
    for(int i=0;i<countSensors;i++){
      if(!sensors.getAddress(thermometerAddress, i)){
        continue;
      }
      payload += ":t";
      for(int j=0;j<8;j++){
        payload += String(thermometerAddress[j], HEX);
      }
      payload += "=";
      float temperature = sensors.getTempC(thermometerAddress);
      payload += String(temperature);
    }
    if(countSensors>0){
        DEBUGLOG("Term count="+ countSensors);
      char *buf = new char[payload.length()+2];
      payload.toCharArray(buf, payload.length()+2);
      if(!webapiServer.tcpNotify(buf)){
        webapiServer.udpNotify(buf);
      };
      delete buf;
    }else
    {
        DEBUGLOG("No therms.");
    }
    
    return;
  }
  if( millis() > nextTemeratureAsk){
    nextTemeratureAsk = millis() + periodTemperatureAsk;
    if(!sensors.getDeviceCount()){
        DEBUGLOG("Start scan therms");
      sensors.begin(); // rescan devices
      delay(10);
      DEBUGLOG(sensors.getAddress(thermometerAddress, 0) ? "therm0 addr got" : "No addr therm");

      return;
    }
    DEBUGLOG("start measure temperature");
    sensors.requestTemperatures();
    whenTemperatureReady = millis() + 750;
  }
}
#endif
void loop(){
  webapiServer.yield();
  server.handleClient();
  yield();
  ftpSrv.handleFTP();
  yield();
  #ifdef ENABLE_TEMP
    handleThermometers();
  #endif
  for(int8_t i=0; i<BUTTONS_COUNT; i++){
    buttons[i].read(i);
    yield();
  }
  yield();
}