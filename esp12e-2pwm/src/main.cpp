#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>
// #include <FS.h>
// #include <OneWire.h>

#include <VVF.h>
#include <SimpleButton.h>

using namespace simplebutton;

#include "Effects.h"

#ifdef ENABLE_OTA
#include <ESP8266HTTPUpdateServer.h>
#endif

#include "wifi_auth.h"
const char* ssid = WIFI_SSID;
const char* password = WIFI_PASSWORD;

ESP8266WebServer server(80);
#ifdef ENABLE_OTA
ESP8266HTTPUpdateServer httpUpdater;
#endif

uint8_t pwmPins[] = {5,4};
Effects e(pwmPins, 2);

int maxOnButtonValue = MAXVALUE;
#define TIMEOUT_COUNT 50

#define DEBOUNCE_MS 80

bool lockBtns = false;
#define INIT_BUTTON(pin) Button(pin, true)
Button buttons[]={
  INIT_BUTTON(16),
  INIT_BUTTON(14),
  INIT_BUTTON(12),
  INIT_BUTTON(13)
};
const char * eventNames[] = {"down:on", "up:", "down:off"};


VVFsWebAPI webapiServer(ssid, password);

void setPWMStatus(uint8_t devNo, int value){
  e.soft(devNo, value);
};

int getPWMStatus(uint8_t devNo){
  return (int)e.getValue(devNo);
}

int getButton(int8_t btnNo){
  return buttons[btnNo].getState() ? maxOnButtonValue : 0;
}

void check_buttons(){
  for(uint8_t i=0;i<2;i++){
    buttons[i].update();
    uint8_t eventId = 0;
    int16_t stripBright = e.getValue(i);
    if (buttons[i].pushed()){
      if (e.getValue(i)>=maxOnButtonValue){
        eventId = 3;
        e.soft(i, 0);
      } else {
        eventId = 1;
        e.soft(i, maxOnButtonValue);
      }
    }else if(buttons[i].released()){
      eventId = 2;
    }

    if(eventId){
      String res = "";
      res += eventId-1;
      res += ":";
      res += i;
      res += ":";
      res += eventNames[eventId - 1];
      res += ":";
      res += stripBright;
      webapiServer.anyNotify(res.c_str());
    }
  }
}

void setup() {
  Serial.begin(115200);
  #ifdef DEBUG
  for(int i=0;i<15;i++){
    delay(1000);
    Serial.println(15-i);
  }
  Serial.println(" Start");
  #endif
  // 834539
  pinMode(5, OUTPUT);
  pinMode(4, OUTPUT);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");


  webapiServer.setDeviceFn(setPWMStatus, getPWMStatus);
  webapiServer.setupButtons(2, getButton);
  webapiServer.setup(&server, 2);
  #ifdef ENABLE_OTA
  #ifdef DEBUG
  Serial.println("OTA setup");
  #endif
  httpUpdater.setup(&server);
  #endif

  server.on("/restart", [&]{
    if(server.method() == HTTP_POST){
      server.send(200, "text/html", "<h1>Restarting...</h1>");
      yield();
      ESP.restart();
      return;
    }
    server.send(200, "text/html", "<form method='post'><input type='submit' value='restart'></form>");
    yield();
  });
  server.on("/all-on", [&]{
    e.turnOnImmediate();
    server.send(200, "text/plain", "Ok");
    yield();
  });

  server.on("/all-off", [&]{
    e.turnOffImmediate();
    server.send(200, "text/plain", "Ok");
    yield();
  });
  server.on("/dest", [&]{
    String resp("{");
    for(int8_t i=0; i<2; i++){
      if(i){
        resp += ",";
      }
      resp +="\"d";
      resp +=i;
      resp +="\":";
      resp += e.getDestinationValue(i);
  }
  resp += "}";
  server.send(200,"text/json", resp);
  });


  server.on("/set-speed", [&]{
    int16_t speed=400;
    String v=server.arg("speed");
    if(v.length()){
      speed = v.toInt();
      e.setSoftSpeed(speed);
      server.send(200, "text/plain", "Ok");
    }else{
      server.send(200, "text/plain", "ND");
    }
    yield();
  });

  server.on("/set-max", [&]{
    String v=server.arg("max");
    if(v.length()){
      maxOnButtonValue = v.toInt();
      if(maxOnButtonValue<=0){
        maxOnButtonValue = 1;
      }
      server.send(200, "text/plain", "Ok");
    }else{
      server.send(200, "text/plain", "ND");
    }
    yield();
  });

  e.setSoftSpeed(200);
  e.begin();
  server.begin();
}

void loop() {
  e.yield();
  webapiServer.yield();
  server.handleClient();
  check_buttons();
  yield();
}
