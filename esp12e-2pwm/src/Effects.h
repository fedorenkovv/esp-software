#ifndef Effects_h
#define Effects_h
#include <Arduino.h>

#define MAXVALUE 100

// PWMRANGE
class Effects {
  uint8_t * pins;
  uint8_t pinCount;

  int16_t speedDelay;
  uint64_t nextStepAt;
  uint8_t * current;
  uint8_t * stopAt;

  int16_t softSpeed;


  void apply();
  void doStep();
  void doStepPin(int8_t i);

public:
  static uint16_t valuesTranslate[MAXVALUE];

  Effects(uint8_t * pins, uint8_t pinCount);

  void begin();

  void yield();

  void turnOffImmediate();
  void turnOnImmediate();

  void setSoftSpeed(int16_t speed);
  void soft(int8_t pinNo, uint8_t dirLights);
  void setValue(int8_t devNo, uint16_t newValue);
  int16_t getValue(int8_t devNo);
  int16_t getDestinationValue(int8_t devNo);

  bool loop;

  // void turnOnConsistently();
  // void turnOnConsistentlyFromHalf();
  // void turnOnTogether();
  //
  // void turnOffConsistently();
  // void turnOffConsistentlyFromHalf();
  // void turnOffTogether();
};

#endif
