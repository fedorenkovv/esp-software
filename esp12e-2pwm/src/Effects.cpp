#include "Effects.h"

uint16_t Effects::valuesTranslate[]={
  0, 1, 2, 3,
  4, 8, 11, 15, 19, 22, 26, 30, 34, 38, 42, 47, 51, 55, 60, 64, 69, 74, 79, 84, 89, 94, 100, 105, 111, 117, 123, 128, 135, 141, 147, 154, 160, 167, 174, 181, 188, 196, 203, 211, 219, 227, 235, 244, 252, 261, 270, 279, 288, 298, 308, 318, 328, 338, 349, 360, 371, 382, 394, 406, 418, 430, 443, 455, 469, 482, 496, 510, 524, 539, 553, 569, 584, 600, 616, 633, 650, 667, 685, 703, 722, 740, 760, 779, 799, 820, 841, 862, 884, 907, 930, 953, 977, 1001
  ,PWMRANGE, PWMRANGE
};

Effects::Effects(uint8_t * pins, uint8_t pinCount){
  this->pins = pins;
  this->pinCount = pinCount;
  nextStepAt = 0;
  softSpeed = 400;
  current = new uint8_t[pinCount];
  stopAt = new  uint8_t[pinCount];
  memset(current, 0, pinCount*sizeof(uint16_t));
  for(int8_t i =0;i<pinCount; i++){
    stopAt[i] = 0;
  }
};

void Effects::begin(){
  #ifdef DEBUG

  Serial.println("init pins:");
  #endif
  for(int8_t i=0;i<pinCount; i++){
    yield();
    #ifdef DEBUG
    Serial.print(i);
    Serial.print(":");
    Serial.print(pins[i]);
    Serial.println(",");
    #endif
    pinMode(pins[i], OUTPUT);
    digitalWrite(pins[i], 0);
  }
  yield();
  #ifdef DEBUG
  Serial.println("init pins done");
  #endif
  return;
}

void Effects::setSoftSpeed(int16_t speed){
    softSpeed = speed;
}

void Effects::soft(int8_t pinNo, uint8_t newValue){
    speedDelay = 8192 / softSpeed;
    if(speedDelay < 3){
      speedDelay = 3;
    }
    nextStepAt = millis()+10;
    if(newValue > MAXVALUE){
      newValue = MAXVALUE;
    }
    stopAt[pinNo] = newValue;
}

void Effects::yield(){
  unsigned long _now = millis();
  if(nextStepAt && _now >= nextStepAt){
      doStep();
      ::yield();
    nextStepAt += speedDelay;
  }
}

void Effects::doStepPin(int8_t i){
  if(current[i] == stopAt[i]){
    return;
  }
  if(current[i] < stopAt[i]){
    current[i] ++;
  }else{
    current[i] --;
  }
}

void Effects::doStep(){
  for(int8_t i=0;i<pinCount;i++){
    doStepPin(i);
  }
  apply();
}

void Effects::apply() {
  for(size_t i=0; i < pinCount; i++){
    if(current[i] == 0){
      analogWrite(pins[i], 0);
      digitalWrite(pins[i],
        #ifdef INVERT_OUTPUT
        LOW
        #else
        HIGH
        #endif
      );
    }else
    if(current[i]<= MAXVALUE-1){
      #ifdef PWM_LINEAR
      uint16_t realValue = current[i]<<2;
      #else
      uint16_t realValue = valuesTranslate[current[i]];
      #endif
      if(realValue > PWMRANGE){
        realValue = PWMRANGE;
      }
      analogWrite(pins[i],
      #ifdef INVERT_OUTPUT
        realValue
      #else
        PWMRANGE-realValue
      #endif
      );
    }else
    if(current[i] > MAXVALUE-1){
      analogWrite(pins[i], 0);
      digitalWrite(pins[i],
        #ifndef INVERT_OUTPUT
        LOW
        #else
        HIGH
        #endif
      );
    }
  }
}

void Effects::turnOffImmediate(){

  for(int8_t i=0; i < pinCount; i++){
    current[i] = 0;
    analogWrite(pins[i], 0);
    digitalWrite(pins[i],
      #ifdef INVERT_OUTPUT
      LOW
      #else
      HIGH
      #endif
    );
  }
  nextStepAt = 0;

}

void Effects::turnOnImmediate(){
  for(int8_t i=0; i < pinCount; i++){
    current[i] =  MAXVALUE;
    analogWrite(pins[i], 0);
    digitalWrite(pins[i],
      #ifndef INVERT_OUTPUT
      LOW
      #else
      HIGH
      #endif
    );
  }
  nextStepAt = 0;
}

void Effects::setValue(int8_t devNo, uint16_t newValue){
  if(devNo>pinCount){
    return;
  }
  if(newValue> MAXVALUE) newValue =  MAXVALUE;
  current[devNo] = newValue;
  uint16_t realValue = valuesTranslate[newValue];
  if(realValue > PWMRANGE){
    realValue = PWMRANGE;
  }

  analogWrite(devNo,
    #ifdef INVERT_OUTPUT
    realValue
    #else
    PWMRANGE-realValue
    #endif
  );
}

int16_t Effects::getValue(int8_t devNo){
  return current[devNo];
}
int16_t Effects::getDestinationValue(int8_t devNo){
  return stopAt[devNo];
}


// void turnOnConsistently();
// void turnOnConsistentlyFromHalf();
// void turnOnTogether();
//
// void turnOffConsistently();
// void turnOffConsistentlyFromHalf();
// void turnOffTogether();
